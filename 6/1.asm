INCLUDE Irvine32.inc

.data
	x dword ?
	y dword ?
	z dword ?
	sum dword ?
	inputx BYTE "������x:",0
	inputy BYTE "������y:",0
	inputz BYTE "������z:",0

.code
main PROC
	call input
	push z
	push y
	push x
	call cal
	mov eax,sum
	call WriteInt
	add esp,8
	exit
main ENDP

input PROC
	pushad
	mov	edx,OFFSET inputx
	call WriteString
	call ReadInt
	mov x,eax

	mov	edx,OFFSET inputy
	call WriteString
	call ReadInt
	mov y,eax	

	mov	edx,OFFSET inputz
	call WriteString
	call ReadInt
	mov z,eax

	popad
	ret
input ENDP

cal proc
	push ebp
	mov ebp,esp
	mov ecx,[ebp+16] ; z
	dec ecx
	mov eax,[ebp+12] ; y
	add eax,[ebp+8] ; x
	mov sum,eax ; x+y
	.IF ecx !=0
	L2:
		mul sum
		loop L2
	.ENDIF
	mov sum,eax
	pop ebp
	ret 8
cal endp	
END main