INCLUDE Irvine32.inc

.data
	wordArray WORD 0001h,0010h,0100h,1000h

.code
main PROC
	cld 
	mov ax, 0100h 
	mov ecx, lengthof wordArray 
	mov edi, offset wordArray 
	repne scasw 
	sub edi, 4
	mov eax, edi
	call DumpRegs
	call Crlf
	exit
main ENDP
END main
