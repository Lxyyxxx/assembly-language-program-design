.386
.model flat, stdcall
.stack 4096
ExitProcess PROTO, dwExitCode: DWORD

.data
	_myWords LABEL DWORD
	myWords WORD 3 DUP(?),2000h

.code
main PROC
	mov eax,_myWords
	
	INVOKE ExitProcess, 0
main ENDP
END main
