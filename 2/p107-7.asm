.386
.model flat, stdcall
.stack 4096
ExitProcess PROTO, dwExitCode: DWORD

.data
	source BYTE "This is the source string",0
	target BYTE SIZEOF source DUP('#')

.code
main PROC
	mov esi,SIZEOF source
	sub esi,2 ; source���
	mov edi,0 ; target��ͷ
	mov ecx,LENGTHOF source
	L1:
		mov al,source[esi]
		mov target[edi],al
		dec esi
		inc edi
		loop L1 
	INVOKE ExitProcess, 0
main ENDP
END main
