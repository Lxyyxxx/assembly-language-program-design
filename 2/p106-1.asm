.386
.model flat, stdcall
.stack 4096
ExitProcess PROTO, dwExitCode: DWORD

.data
	three DWORD 12345678h

.code
main PROC
	mov esi,OFFSET three
	mov ax,WORD PTR [three+2]
	mov bx,WORD PTR [three]
	mov WORD PTR three+2,bx
	mov WORD PTR three,ax
	
	INVOKE ExitProcess, 0
main ENDP
END main
