.386
.model flat, stdcall
.stack 4096
ExitProcess PROTO, dwExitCode: DWORD

.data
	array DWORD 1,2,3,4,5,6

.code
main PROC
	mov esi,0 
	mov ecx,LENGTHOF array
	shr ecx,1 ; 循环次数为个数的一半
	L1:
		mov eax,array[esi]
		mov ebx,array[esi+4]
		mov array[esi+4],eax 
		mov array[esi],ebx
		add esi,8
		loop L1 

	INVOKE ExitProcess, 0
main ENDP
END main
