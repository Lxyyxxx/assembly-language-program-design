INCLUDE Irvine32.inc

.data
	op1 dword 12345678h,90132435h
	op2 dword 13243546h,57687912h
	result dword 2 dup(0)

.code
main PROC
	mov esi,offset op1
	mov edi,offset op2
	mov ebx,offset result
	mov ecx,lengthof op1
	call Extended_Sub
	mov ecx,lengthof result ; display
	sub ecx,1
L1:
	mov eax,ecx
	mov edx,4
	mul edx
	mov edx,DWORD ptr [ebx + eax]
	mov eax,edx
	call WriteHex
	loop L1
	mov eax,DWORD ptr [ebx]
	call WriteHex
	call Crlf
	exit
main ENDP


Extended_Sub PROC
	pushad
	clc
L1:
	mov eax,[esi]
	sub eax,[edi]
	pushfd
	mov [ebx],eax
	add esi,4
	add edi,4
	add ebx,4
	popfd
	loop L1
	
	mov dword ptr [ebx],0
	sbb dword ptr [ebx],0
	popad
	ret
Extended_Sub ENDP
END main