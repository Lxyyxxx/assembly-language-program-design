INCLUDE Irvine32.inc

.data
	val1 SDWORD 10
	val2 SDWORD 40
	val3 SDWORD -4

.code
main PROC
	mov ebx,val1
	add ebx,val2 ; val1+val2
	mov eax,val2
	cdq
	mov ecx,val3
	idiv ecx ; val2/val3
	imul ebx,eax
	call DumpRegs
	mov val1,ebx  
	mov eax,val1
	call WriteInt
	call Crlf
	exit
main ENDP
END main