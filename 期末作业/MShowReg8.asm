INCLUDE Irvine32.inc
; macro
MShowReg8 MACRO reg:REQ
	LOCAL tempOutput
	.data
		tempOutput BYTE "&reg=",0
	.code
		push eax
		push ebx
		; output tempOutput
		push edx
		mov	edx,OFFSET tempOutput
		call WriteString
		pop	edx
		; output reg value
		movzx eax,reg
		mov ebx,1
		call WriteHexB
		call Crlf
		pop ebx
		pop	eax
ENDM
; test program
.code
main PROC
	MShowReg8 AH
	MShowReg8 AL
	MShowReg8 bh
	MShowReg8 bl
	MShowReg8 Ch
	MShowReg8 Cl
	MShowReg8 dH
	MShowReg8 dL
	exit
main ENDP
END main
