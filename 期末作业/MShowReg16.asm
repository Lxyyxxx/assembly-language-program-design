INCLUDE Irvine32.inc
; macro
MShowReg16 MACRO reg:REQ
	LOCAL tempOutput
	.data
		tempOutput BYTE "&reg=",0
	.code
		push eax
		push ebx
		; output tempOutput
		push edx
		mov	edx,OFFSET tempOutput
		call WriteString
		pop	edx
		; output reg value
		movzx eax,reg
		mov ebx,2
		call WriteHexB
		call Crlf
		pop ebx
		pop	eax
ENDM
; test program
.code
main PROC
	MShowReg16 AX
	MShowReg16 bx
	MShowReg16 Cx
	MShowReg16 dX
	MShowReg16 SI
	MShowReg16 DI
	MShowReg16 BP
	MShowReg16 SP
	exit
main ENDP
END main
