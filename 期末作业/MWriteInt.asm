INCLUDE Irvine32.inc
; macro
MReadInt MACRO var:REQ
	LOCAL inputInfo
	.data
		inputInfo BYTE "input a signed decimal integer:",0
	.code
		push eax
		; output inputInfo
		push edx
		mov	edx,OFFSET inputInfo
		call WriteString
		pop	edx
		; move to var
		call ReadInt
		mov	var,eax
		pop	eax
ENDM

MWriteInt MACRO var:REQ
	LOCAL outputInfo
	.data
		outputInfo BYTE "&var=",0
	.code
		push eax
		; output outputInfo
		push edx
		mov	edx,OFFSET outputInfo
		call WriteString
		pop	edx
		; output var
		mov eax,var
		call WriteInt
		call Crlf
		pop	eax
ENDM
; test program
.data
	_int SDWORD ?
.code
main PROC
	MReadInt _int
	MWriteInt _int
	MReadInt ecx
	MWriteInt ecx
	exit
main ENDP
END main
