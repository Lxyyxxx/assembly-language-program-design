INCLUDE Irvine32.inc
; macro
MReadInt MACRO var:REQ
	LOCAL inputInfo
	.data
		inputInfo BYTE "input a signed decimal integer:",0
	.code
		push eax
		; output inputInfo
		push edx
		mov	edx,OFFSET inputInfo
		call WriteString
		pop	edx
		; move to var
		call ReadInt
		mov	var,eax
		pop	eax
ENDM
; test program
.data
	_int SDWORD ?
.code
main PROC
	MReadInt _int
	; output _int
	push eax
	mov eax,_int
	call WriteInt
	call Crlf
pop	eax
	exit
main ENDP
END main
