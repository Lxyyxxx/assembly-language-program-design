INCLUDE Irvine32.inc
; macro
MShowDivRes MACRO bit:REQ
	LOCAL outputInfo,outputQuotient,outputRemainde
	.data
		outputInfo BYTE "the &bit-bit div result:",0
		outputQuotient BYTE "	quotient=",0
		outputRemainde BYTE "	remainder=",0
	.code
		push eax
		; output outputInfo
		push edx
		mov	edx,OFFSET outputInfo
		call WriteString
		pop	edx
		; output mul result
		IF bit EQ 8
			; output quotient al and remainder ah
			; output outputQuotient
			push edx
			mov	edx,OFFSET outputQuotient
			call WriteString
			pop	edx
			; output quotient al
			push eax ; save ah
			movzx eax,al
			call WriteDec
			; output outputRemainde
			push edx
			mov	edx,OFFSET outputRemainde
			call WriteString
			pop	edx
			; output quotient ah
			pop eax
			movzx eax,ah
			call WriteDec
		ELSEIF bit EQ 16
			; output quotient ax and remainder dx
			; output outputQuotient
			push edx
			mov	edx,OFFSET outputQuotient
			call WriteString
			pop	edx
			; output quotient ax
			cwde ; ax -> eax
			call WriteDec
			; output outputRemainde
			push edx
			mov	edx,OFFSET outputRemainde
			call WriteString
			pop	edx
			; output quotient dx
			movzx eax,dx
			call WriteDec
		ELSEIF bit EQ 32
			; output quotient eax and remainder edx
			; output outputQuotient
			push edx
			mov	edx,OFFSET outputQuotient
			call WriteString
			pop	edx
			; output quotient eax
			call WriteDec
			; output outputRemainde
			push edx
			mov	edx,OFFSET outputRemainde
			call WriteString
			pop	edx
			; output quotient edx
			mov eax,edx
			call WriteDec
		ENDIF
		call Crlf
		pop	eax
ENDM
; test program
.code
main PROC
	; 8-bit div ax
	mov ax,601
	mov bl,5
	div bl
	MShowDivRes 8
	; 16-bit div dx:ax
	mov dx,0002h ; dec = 163502
	mov ax,7EAEh
	movzx bx,bl
	div bx
	MShowDivRes 16
	; 32-bit div edx:eax
	mov edx,00000002h ; dec = 10737418003
	mov eax,7FFFFF13h
	movzx ebx,bx
	div ebx
	MShowDivRes 32
	exit
main ENDP
END main
