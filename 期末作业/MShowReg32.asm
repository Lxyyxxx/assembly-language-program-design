INCLUDE Irvine32.inc
; macro
MShowReg32 MACRO reg:REQ
	LOCAL tempOutput
	.data
		tempOutput BYTE "&reg=",0
	.code
		push eax
		; output tempOutput
		push edx
		mov	edx,OFFSET tempOutput
		call WriteString
		pop	edx
		; output reg value
		mov	eax,reg
		call WriteHex
		call Crlf
		pop	eax
ENDM
; test program
.code
main PROC
	MShowReg32 EAX
	MShowReg32 ebx
	MShowReg32 eCX
	MShowReg32 Edx
	MShowReg32 ESI
	MShowReg32 EDI
	MShowReg32 EBP
	MShowReg32 ESP
	exit
main ENDP
END main
