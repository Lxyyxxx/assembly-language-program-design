INCLUDE Irvine32.inc
; macro
MShowMulRes MACRO bit:REQ
	LOCAL outputInfo,_index,_res,_low,_high,_eax_q,_eax_r,_edx_q,_edx_r,_q,_r,_dw10
	.data
		outputInfo BYTE "the &bit-bit mul result=",0
		_index dword 0
		_res byte 64 dup(?)
		_low dword ?
		_high dword ?
		_eax_q dword  0
		_eax_r dword  0
		_edx_q dword  0
		_edx_r dword  0
		_q dword 429496729 ; 2 ^ 32 = 429496729
		_r dword 6
		_dw10 dword 10
	.code
		push eax
		; output outputInfo
		push edx
		mov	edx,OFFSET outputInfo
		call WriteString
		pop	edx
		; output mul result
		IF bit EQ 8
			; output ax
			movzx eax,ax
			call WriteDec
		ELSEIF bit EQ 16
			; output dx:ax
			shl edx,16
			mov dx,ax
			mov eax,edx
			call WriteDec
		ELSEIF bit EQ 32
			; output edx:eax
			pushad
			mov _low,eax
			mov _high,edx
		.WHILE (_low!=0) || (_high!=0)
			; edx
			mov edx,0
			mov eax,_high
			div _dw10
			mov [_edx_r],edx ; edx % result
			mov [_edx_q],eax ; edx / result
			; eax
			mov edx,0
			mov eax,_low
			div _dw10
			mov [_eax_r],edx ; eax % result
			mov [_eax_q],eax ; eax / result
			; r = _edx_r * _r + _eax_r
			mov eax,_r
			mul [_edx_r]
			add eax,[_eax_r]
			; r / 10
			cdq ; eax -> edx:eax
			div _dw10
			inc _index
			; save
			mov ebx,_index
			mov [_res+ebx],dl ; r % 10
			mov [_low],eax
			mov [_high],0
			; 10 * _q * _edx_q
			mov eax,4294967290 ; 10 * _q = 4294967290
			mul [_edx_q]
			add [_low],eax
			adc [_high],edx
			; _q *  _edx_r
			mov eax,_q
			mul _edx_r
			add [_low],eax
			adc [_high],edx
			; _r * _edx_q
			mov eax,_r
			mul _edx_q
			add [_low],eax
			adc [_high],edx
			; _eax_q
			mov eax,_eax_q
			add [_low],eax
			adc [_high],0
			mov ecx,_index
		.ENDW
		print:
			movzx eax,[_res+ecx]
			call WriteDec
			loop print
			popad
		ENDIF
		call Crlf
		pop	eax
ENDM
; test program
.code
main PROC
	; 8-bit mul
	mov al,120
	mov bl,5
	mul bl
	MShowMulRes 8
	; 16-bit mul
	mov ax,32700
	mov bx,5
	mul bx
	MShowMulRes 16
	; 32-bit mul
	mov eax,2147483600
	mov ebx,5
	mul ebx
	MShowMulRes 32
	exit
main ENDP
END main
