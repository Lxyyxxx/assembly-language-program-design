INCLUDE Irvine32.inc
; macro
MShowCurrentTime MACRO xy:REQ
	LOCAL sysTime,consoleHandle,colonStr,exitTips
	.data
		sysTime SYSTEMTIME <>
		consoleHandle DWORD ?
		colonStr BYTE ":",0
		exitTips BYTE "Press Ctrl+C to exit...",0
	.code
		.WHILE 1 ; 996ms fresh
			INVOKE GetStdHandle,STD_OUTPUT_HANDLE
			mov consoleHandle,eax
			INVOKE SetConsoleCursorPosition,consoleHandle,xy
			INVOKE GetLocalTime,ADDR sysTime
			movzx eax,sysTime.wHour
			call WriteDec ; without leading zero
			mov edx,offset colonStr
			call WriteString
			movzx eax,sysTime.wMinute
			call WriteDec
			call WriteString                
			movzx eax,sysTime.wSecond	
			call WriteDec
			call Crlf
			call Crlf
			mov edx,offset exitTips
			call WriteString
			call Crlf
			mov eax,996
			call Delay
	.ENDW
ENDM
; test program
.data
	_xy COORD <5,1>
.code
main PROC
	MShowCurrentTime _xy
	exit
main ENDP
END main
