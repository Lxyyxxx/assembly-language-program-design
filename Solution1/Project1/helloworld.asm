; 输出Hello World
; 关闭McAfee实时扫描即可运行

INCLUDE Irvine32.inc

.data
    str1 BYTE "Hello, world!", 0

.code
main PROC
    mov edx, OFFSET str1
    call WriteString
    call Crlf

    ; INVOKE ExitProcess, 0 ; 可设断点
	exit ; 不可设断点
main ENDP
END main
