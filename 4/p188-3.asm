INCLUDE Irvine32.inc

.data
	score DWORD ?
.code
main PROC
	mov	ecx,10
	rand:
		mov eax,51
		call RandomRange
		add eax,50
		call WriteInt
		mov score,eax
		mov al,' '
		call WriteChar
		call CalcGrade
		call Crlf
		loop rand
	exit
main ENDP
CalcGrade PROC
	mov eax,score
	cmp eax,90 
	jge fa ; >=90
	cmp eax,80
	jge fb ; >=80
	cmp eax,70
	jge fc ; >=70
	cmp eax,60
	jge fd ; >=60
	ff: ; <=59
		mov al,'F'
		call WriteChar
		ret
	fd: 
		mov al,'D'
		call WriteChar
		ret
	fc: 
		mov al,'C'
		call WriteChar
		ret
	fb: 
		mov al,'B'
		call WriteChar
		ret
	fa:
		mov al, 'A' 
		call WriteChar
		ret
	ret
CalcGrade ENDP
END main