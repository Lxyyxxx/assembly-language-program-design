; 链接库测试 #2 生成随机整数
; 测试 Randomize, Random32 和 RandomRange 过程
INCLUDE Irvine32.inc

TAB = 9         ; Tab 的 ASCII 码

.code
main PROC
    call Randomize      ; 初始化伪随机数生成器
    call Rand1
    call Rand2
    exit
main ENDP

Rand1 PROC
; 生成 10 个伪随机整数
    mov ecx, 10         ; 循环 10 次
L1:
    call Random32       ; 生成随机整数
    call WriteDec       ; 用无符号十进制形式输出
    mov al, TAB         ; 水平制表符
    call WriteChar      ; 输出制表符
    Loop L1

    call Crlf
    ret
Rand1 ENDP

Rand2 PROC
; 在 -50 到 +49 之间生成 10 个伪随机整数
    mov ecx, 10         ; 循环 10 次
L1:
    ;mov eax, 100        ; 数值范围 0~99
	mov eax, 45      ; 数值范围 0~45
    call RandomRange    ; 生成随机整数
    ;sub eax, 50         ; 数值范围 -50~+49
	add eax, 15         ; 数值范围 15~60
    call WriteInt       ; 用有符号十进制形式输出
    mov al, TAB         
    call WriteChar      ; 输出制表符
    loop L1

    call Crlf
    ret 
Rand2 ENDP

END main
