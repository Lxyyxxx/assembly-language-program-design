; 库测试 #1: 整数 I/O
; 测试 Clrscr, DumpMem, ReadInt, SetTextColor, 
; WaitMsg, WriteBin, WriteHex 和 WriteString 过程
INCLUDE Irvine32.inc

.data
    COUNT = 4
    ;BlueTextOnGray = blue + (lightGray * 16)
	RedTextOnWhite = red + (white * 16)
    DefaultColor = lightGray + (black * 16)
    arrayD SDWORD 12345678h, 1A4B2000h, 3434h, 7AB9h
    prompt BYTE "Enter a 32-bit signed integer: ", 0

.code
main PROC
    ; 选择浅灰色背景蓝色文本
    ;mov eax, BlueTextOnGray
	; 选择白底红色字体
	mov eax, RedTextOnWhite
    call SetTextColor
    call Clrscr                     ; 清屏, 以应用新设置的颜色
    
    ; 用 DumpMem 显示 arrayD 数组的内存区
    mov esi, OFFSET arrayD
    mov ebx, TYPE arrayD
    mov ecx, LENGTHOF arrayD
    call DumpMem

    ; 请求用户输入 4 个有符号整数
    call Crlf
    mov ecx, COUNT
L1:
    mov edx, OFFSET prompt
    call WriteString
    call ReadInt                    ; 输入数据存入 EAX
    call Crlf
    call WriteInt                   ; 显示为有符号十进制
    call Crlf
    call WriteHex                   ; 显示为十六进制
    call Crlf
    call WriteBin                   ; 显示为二进制
    call Crlf
    call Crlf
    Loop L1
    call WaitMsg                    ; 显示 "Press any key..." 并等待用户按键

    ; 将控制台窗口属性返回默认颜色(黑色背景线灰色字符)
    mov eax, DefaultColor
    call SetTextColor
    call Clrscr

    exit
    
main ENDP
END main
