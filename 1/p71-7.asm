.386
.model flat, stdcall
.stack 4096
ExitProcess PROTO, dwExitCode: DWORD

.data
	array DWORD 120 DUP(?) ; 120个未初始化无符号双字数组

.code
main PROC
	mov eax, array

	INVOKE ExitProcess, 0
main ENDP
END main
