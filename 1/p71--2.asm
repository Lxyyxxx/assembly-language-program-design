.386
.model flat, stdcall
.stack 4096
ExitProcess PROTO, dwExitCode: DWORD

; 符号常量
Monday    = 1
Tuesday   = 2
Wednesday = 3
Thursday  = 4
Friday    = 5
Saturday  = 6
Sunday    = 7

.data 
	week DWORD Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday ; 数组变量

.code
main PROC
	mov eax, week
	INVOKE ExitProcess, 0
main ENDP
END main
