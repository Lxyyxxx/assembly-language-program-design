.386
.model flat, stdcall
.stack 4096
ExitProcess PROTO, dwExitCode: DWORD

.data
	value1 DWORD 25 ; 25的十进制
	value2 DWORD 00011001b ; 25的二进制
	value3 DWORD 31o ; 25的八进制
	value4 DWORD 19h ; 25的十六进制
.code
main PROC
	
	INVOKE ExitProcess, 0
main ENDP
END main
