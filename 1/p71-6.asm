.386
.model flat, stdcall
.stack 4096
ExitProcess PROTO, dwExitCode: DWORD

.data
	value1 DWORD 456789ABh

.code
main PROC
	mov eax, value1

	INVOKE ExitProcess, 0
main ENDP
END main
