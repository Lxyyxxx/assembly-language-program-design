.386
.model flat, stdcall
.stack 4096
ExitProcess PROTO, dwExitCode: DWORD

.data
	sum DWORD 0

.code ; 此为代码区 
main PROC
	mov eax, 5 ; 将数字 5 送入 eax 寄存器 
	add eax, 6 ; eax 寄存器加 6 
	mov sum, eax ; 将求和结果保存至sum
	
	INVOKE ExitProcess, 0 ; 程序结束 
main ENDP
END main
