.386
.model flat, stdcall
.stack 4096
ExitProcess PROTO, dwExitCode: DWORD

.data
	dArray SDWORD 50 DUP(?) ; 未初始化数组，50个有符号双字

.code
main PROC

	mov eax, dArray
	INVOKE ExitProcess, 0
main ENDP
END main
